import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import { createApp } from 'vue';

const app = createApp({});

export default function () {
  app.use(window.bootstrap);
}