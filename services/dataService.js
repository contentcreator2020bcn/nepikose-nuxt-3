/** Get parsed data from sessionStorage */
import dataEn from '../static/data.json'
function getDataFromSession() {
  return dataEn
}

/** Get list of all catgories */
function getCategories() {
  let data = getDataFromSession();
  let categoryIDsInPartners = new Set(data.partners.map(partner => partner.type));
  let categoriesInPartners = data.categories.filter(category => categoryIDsInPartners.has(category.type));
  return categoriesInPartners;
}

/** Get list of all partners of a specific type */
function getPartnersByType(type) {
  let data = getDataFromSession();
  return sortJSONObjectList(data.partners.filter(partner => partner.type === type));
}

/** Get a single partner given the partner id */
function getPartnerById(partnerId) {
  let partners = getDataFromSession().partners;
  return partners.find(partner => partner.id === partnerId);
}

/** Get all partners */
function getPartners(partnerId) {
  return getDataFromSession().partners;
}

/** Get all requests */
function getRequests(partnerId) {
  return getDataFromSession().requests;
}

function sortJSONObjectList(jsonList) {
  jsonList.sort(function (a, b) {
    if (a.description && !b.description) {
      return -1; // `a` has a description and `b` does not, so `a` should come before `b`
    } else if (!a.description && b.description) {
      return 1; // `b` has a description and `a` does not, so `b` should come before `a`
    } else {
      return 0; // No difference in description presence, maintain the current order
    }
  });

  return jsonList;
}

export default {
  getDataFromSession,
  getCategories,
  getPartnersByType,
  getPartnerById,
  getPartners,
  getRequests
}