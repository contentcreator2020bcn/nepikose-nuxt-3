/** Array Utils */
function customArraySort(arrayA, arrayB) {
    return arrayA.slice().sort((a, b) => {
        const indexA = arrayB.indexOf(a.category);
        const indexB = arrayB.indexOf(b.category);
        if (indexA !== -1 && indexB !== -1) {
            return indexA - indexB;
        }
        if (indexA !== -1) {
            return -1;
        }
        if (indexB !== -1) {
            return 1;
        }
        return 0;
    });
}

/** Image Utils */
function getImgUrl(partner) {
    console.log('partner---', partner)
    // return `~/directus/uploads/${partner.logo}.jpeg`;
    // if (partner.logo) {
    // }
    // return require('@/assets/images/dummy.jpeg');
}

export {
    customArraySort,
    getImgUrl,
}
