import { Resend } from 'resend';

const resend = new Resend('re_5VEvnody_P2DdDajjrsRTCm8XcS4SXNFH');

export default defineEventHandler(async () => {
  try {
    const data = await resend.emails.send({
      from: 'testing@sbstudioo.com',
      to: ['chaku.lama@gmail.com'],
      subject: 'Hello world',
      html: '<strong>It works!</strong>',
    });

    return data;
  } catch (error) {
    return { error };
  }
});
