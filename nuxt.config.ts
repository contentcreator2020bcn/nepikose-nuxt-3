// https://nuxt.com/docs/api/configuration/nuxt-config
export default ({
  ssr: false,
  target:'static',
  css: [
    '~/assets/main.scss',
    "~/node_modules/bootstrap/dist/css/bootstrap.min.css",
  ],
  plugins: [
    { src: '~/plugins/bootstrap.js', mode: 'client' }
  ],
  modules: ['nuxt-swiper', 'nuxt-purgecss'],
  app: {
    head: {
      title: 'NepiKose',
      htmlAttrs: {
        lang: 'en'
      },
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: "Explore Barcelona's Nepalese scene: restaurants, shops, and taxi services. Immerse in Nepali culture in Spain, with nationwide expansion ahead. Join our community and find job opportunities at Nepikose.Dive in now!" },
        { name: 'keywords', content: 'Nepikose, Nepalese Job Seekers Barcelona, Barcelona Employment Platform, Job Opportunities for Nepalese in Barcelona, Nepalese Career Network, Barcelona Job Portal, Nepali in Barelona.' },
        { name: 'format-detection', content: 'telephone=no' }
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
      ],
      script: [{ src: 'https://kit.fontawesome.com/ff5631bcc9.js', async: true, crossorigin: 'anonymous' },
      { src: 'https://www.googletagmanager.com/gtag/js?id=G-2374K74HJR', async: true },
      {
        src: "/analytics.js",
      }]
    },
  },
  router: {
    base: '/',
  },
  generate: {
    dir: 'public',
  },

})